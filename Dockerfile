FROM ubuntu:bionic-20191010 as build

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
      build-essential=12.* \
      ca-certificates=20* \
      dh-autoreconf=* \
      dh-systemd=* \
      libssl-dev=1.1.* \
      netbase=* \
      unzip=6.*

WORKDIR /build

ADD https://github.com/asciiprod/yadifa/archive/master.zip master.zip

ARG yadifa_version=2.3.9
ARG yadifa_build=8497
ADD http://yadifa.eu/r/yadifa-${yadifa_version}-${yadifa_build}.tar.gz /build/yadifa_${yadifa_version}.orig.tar.gz

RUN ls -al && ls -al ../ && \
    unzip master.zip

WORKDIR /build/yadifa-master

ARG yadifa_version=2.3.9
RUN dpkg-buildpackage -us -uc && \
    mv /build/yadifa_${yadifa_version}-1_*.deb /build/yadifa.deb

WORKDIR /build/

ARG tini_version=v0.18.0
ADD https://github.com/krallin/tini/releases/download/${tini_version}/tini /build/tini
RUN chmod +x /build/tini


# Now let's build the real thing
FROM ubuntu:bionic-20191010

COPY --from=build /build/yadifa.deb /tmp/yadifa.deb
COPY --from=build /build/tini /tini

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    libssl-dev=1.1.* \
    netbase=* && \
    dpkg -i /tmp/yadifa.deb && \
    mkdir /run/yadifa && chown -R yadifa:yadifa /run/yadifa && \
    apt-get -y clean && rm -rf /var/lib/apt/lists/* /tmp/yadifa*.deb

VOLUME /etc/yadifa/conf.d
VOLUME /var/lib/yadifa
EXPOSE 5353
WORKDIR /var/lib/yadifa

ENTRYPOINT ["/tini", "--"]
CMD [ "/usr/sbin/yadifad", "--nodaemon", "-c", "/etc/yadifa/yadifad.conf", \
      "-u", "yadifa", "-g", "yadifa", "-P", "5353"]
